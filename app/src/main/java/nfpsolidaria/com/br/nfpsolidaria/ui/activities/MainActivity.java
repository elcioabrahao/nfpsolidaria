package nfpsolidaria.com.br.nfpsolidaria.ui.activities;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bluehomestudio.progresswindow.ProgressWindow;
import com.bluehomestudio.progresswindow.ProgressWindowConfiguration;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.google.gson.Gson;
import com.mukesh.image_processing.ImageProcessor;


import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationAction;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadStatusDelegate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import nfpsolidaria.com.br.nfpsolidaria.BuildConfig;
import nfpsolidaria.com.br.nfpsolidaria.R;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.Teste;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.User;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.Nota;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.RestMessageNotas;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.UserLocal;
import nfpsolidaria.com.br.nfpsolidaria.domain.network.RestAPIClient;
import nfpsolidaria.com.br.nfpsolidaria.domain.util.ImageInputHelper;
import nfpsolidaria.com.br.nfpsolidaria.domain.util.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ImageInputHelper.ImageActionListener {

    public static final int NOVO_REGISTRO=1;
    public static final int ALTERAR_REGISTRO=2;
    public static final int SOLICITACAO_NOVO_REGISTRO=33;
    public static final int SOLICITACAO_ALTERAR_REGISTRO=34;
    public static final int RESULTADO_CADASTRO_SUCESSO=1;
    public static final int RESULTADO_CADASTRO_CANCELADO=2;
    public static final String CADASTRO="cadastro";
    private ImageInputHelper imageInputHelper;
    private UserLocal userLocal;
    public User user;
    private ProgressWindow progressWindow;
    CoordinatorLayout coordinatorLayout1;

    public ImageView imagem1;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressConfigurations();
        imagem1 = (ImageView)findViewById(R.id.imagem1);
        coordinatorLayout1 = (CoordinatorLayout)findViewById(R.id.coordinatorLayout1);

        checkAndRequestPermissions();

        imageInputHelper = new ImageInputHelper(this);
        imageInputHelper.setImageActionListener(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        userLocal = UserLocal.findById(UserLocal.class,1);

        if(userLocal ==null){
            Intent intent = new Intent(this,CadastroActivity.class);
            intent.putExtra(CADASTRO,NOVO_REGISTRO);
            startActivityForResult(intent,SOLICITACAO_NOVO_REGISTRO);
        }

        //TODO provisório
        gerarNotas();

    }

    private void progressConfigurations(){
        progressWindow = ProgressWindow.getInstance(this);
        ProgressWindowConfiguration progressWindowConfiguration = new ProgressWindowConfiguration();
        progressWindowConfiguration.backgroundColor = Color.parseColor("#32000000") ;
        progressWindowConfiguration.progressColor = getResources().getColor(R.color.colorPrimary);
        progressWindow.setConfiguration(progressWindowConfiguration);
    }

    public void showProgress(){
        progressWindow.showProgress();
    }


    public void hideProgress(){
        progressWindow.hideProgress();
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideProgress();
    }


    private void gerarNotas(){
        Nota.deleteAll(Nota.class);
        Nota nota;
            nota = new Nota("", "", "15/11/2017 15:15:15", "16/11/2017 15:15:15", "097.186.918-98", 2, 21056, "1", "20171115", 1,1212,"Loja dos Parafusos", "021.234.556/0001-54", "11/11/2017", "1kg de parafusos");nota.save();
            nota = new Nota("", "", "15/11/2017 15:15:15", "16/11/2017 15:15:15", "097.186.918-98", 2, 12455, "1", "20171115", 2,2323,"Loja dos Parafusos", "021.234.556/0001-54", "11/11/2017", "1kg de parafusos");nota.save();
            nota = new Nota("", "", "15/11/2017 15:15:15", "16/11/2017 15:15:15", "097.186.918-98", 2, 34712, "1", "20171115",3,45, "Loja dos Parafusos", "021.234.556/0001-54", "11/11/2017", "1kg de parafusos");nota.save();
            nota = new Nota("", "", "15/12/2017 15:15:15", "16/12/2017 15:15:15", "097.186.918-98", 2, 21056, "1", "20171215", 4,67,"Loja dos Parafusos", "021.234.556/0001-54", "11/11/2017", "1kg de parafusos");nota.save();
            nota = new Nota("", "", "15/12/2017 15:15:15", "16/12/2017 15:15:15", "097.186.918-98", 2, 12455, "1", "20171215",5,67, "Loja dos Parafusos", "021.234.556/0001-54", "11/11/2017", "1kg de parafusos");nota.save();
            nota = new Nota("", "", "15/12/2017 15:15:15", "16/12/2017 15:15:15", "097.186.918-98", 2, 34712, "1", "20171215",7,87, "Loja dos Parafusos", "021.234.556/0001-54", "11/11/2017", "1kg de parafusos");nota.save();
            nota = new Nota("", "", "15/01/2018 15:15:15", "16/01/2018 15:15:15", "097.186.918-98", 2, 21056, "1", "20180115", 8,24,"Loja dos Parafusos", "021.234.556/0001-54", "11/11/2017", "1kg de parafusos");nota.save();
            nota = new Nota("", "", "15/01/2018 15:15:15", "16/01/2018 15:15:15", "097.186.918-98", 2, 12455, "1", "20180115", 9,25,"Loja dos Parafusos", "021.234.556/0001-54", "11/11/2017", "1kg de parafusos");nota.save();
            nota = new Nota("", "", "15/01/2018 15:15:15", "16/01/2018 15:15:15", "097.186.918-98", 2, 34712, "1", "20180115", 10,11,"Loja dos Parafusos", "021.234.556/0001-54", "11/11/2017", "1kg de parafusos");nota.save();
        nota = new Nota("", "", "15/02/2018 15:15:15", "16/02/2018 15:15:15", "097.186.918-98", 2, 211056, "1", "20180215", 8,24,"Loja dos Parafusos", "021.234.556/0001-54", "11/12/2017", "1kg de parafusos");nota.save();
        nota = new Nota("", "", "15/02/2018 15:15:15", "16/02/2018 15:15:15", "097.186.918-98", 2, 121455, "1", "20180215", 9,25,"Loja dos Parafusos", "021.234.556/0001-54", "11/12/2017", "1kg de parafusos");nota.save();
        nota = new Nota("", "", "15/02/2018 15:15:15", "16/02/2018 15:15:15", "097.186.918-98", 2, 347112, "1", "20180215", 10,11,"Loja dos Parafusos", "021.234.556/0001-54", "11/12/2017", "1kg de parafusos");nota.save();

    }

    @Override
    public void onRestart(){
        super.onRestart();
        userLocal = UserLocal.findById(UserLocal.class,1);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.alterar_cadastro) {

            Intent intent = new Intent(this,CadastroActivity.class);
            intent.putExtra(CADASTRO,ALTERAR_REGISTRO);
            startActivityForResult(intent,SOLICITACAO_ALTERAR_REGISTRO);
            return true;
        }
        if (id == R.id.listar_notas) {

            Intent intent = new Intent(this,NotaActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.limpar_notas) {

            Nota.deleteAll(Nota.class);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_doacao) {
            imageInputHelper.takePhotoWithCamera();
        } else if (id == R.id.nav_doacoes_realizadas) {
            Intent intent = new Intent(this,NotaActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_estatisticas) {
            Intent intent = new Intent(this,EstatisticaActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_configuracoes) {
            Intent intent = new Intent(this,SettingsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==SOLICITACAO_NOVO_REGISTRO){
            if(resultCode==RESULTADO_CADASTRO_CANCELADO){
                new MaterialStyledDialog.Builder(this)
                        .setTitle(R.string.tituloPoupUpAgradecimento)
                        .setDescription(R.string.mensagemPoupUpAgradecimento)
                        //.setHeaderDrawable(R.drawable.header)
                        .setStyle(Style.HEADER_WITH_ICON)
                        .withDarkerOverlay(false)
                        //.setIcon(R.drawable.logo_grande_principal)
                        .setHeaderDrawable(R.drawable.logo_cadastro_reduzido)
                        .withDialogAnimation(true)
                        .setCancelable(true)
                        .setNegativeText("Quero sair")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                finish();
                            }
                        })
                        .setPositiveText("Quero doar")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Intent intent = new Intent(MainActivity.this,CadastroActivity.class);
                                intent.putExtra(CADASTRO,NOVO_REGISTRO);
                                startActivityForResult(intent,SOLICITACAO_NOVO_REGISTRO);
                            }
                        })
                        .show();
            }else{

                // prepare for foto

            }

        }else if(requestCode==SOLICITACAO_ALTERAR_REGISTRO){
            if(resultCode==RESULTADO_CADASTRO_CANCELADO){

                Snackbar.make(getCurrentFocus(), "Alteração cencelada!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }else{

                Snackbar.make(getCurrentFocus(), "Alteração feita com sucesso!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }else{

            imageInputHelper.onActivityResult(requestCode, resultCode, data);

        }
    }

    private  boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        int storage = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int storage2 = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int loc = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        int network = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE);
        int internet = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
        //int myLoc = ContextCompat.checkSelfPermission(this, Manifest.permission.LOCATION_HARDWARE);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }
        if (storage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (storage2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (loc2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);

        }
        if (loc != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (network != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_NETWORK_STATE);
        }
        if (internet != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.INTERNET);
        }
        if (!listPermissionsNeeded.isEmpty())
        {
            ActivityCompat.requestPermissions(this,listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public void visualizarDoacoes(View view){

        Intent intent = new Intent(this,NotaActivity.class);
        startActivity(intent);

    }


    public void capturaDaCamera(View view){
        imageInputHelper.takePhotoWithCamera();
    }


    @Override
    public void onImageSelectedFromGallery(Uri uri, File imageFile) {

        imageInputHelper.requestCropImage(uri, 360, 640, 9, 16);
    }

    @Override
    public void onImageTakenFromCamera(Uri uri, File imageFile) {

            //imageInputHelper.requestCropImage(, 360, 640, 9, 16);
        Nota nota = null;
        Bitmap bitmap = null;

        try {

            bitmap = Util.handleSamplingAndRotationBitmap(this,uri);

            imagem1.setImageBitmap(bitmap);

            nota = new Nota();
            String data = Util.getTodayDate();
            nota.setDataEnvio(data);
            nota.setReferencia("cupom_"+BuildConfig.EMPRESA+"_"+userLocal.getCpf().replace(".","").replace("-","")+"_"+Util.getFormatedDateLong(data));
            nota.setArquivo(nota.getReferencia()+".jpg");
            nota.setDataCredito("");
            nota.setStatus(99);
            nota.setUserId(userLocal.getCpf());
            nota.setValor(0L);
            nota.setValorDoado(0L);
            nota.setNotaNumero(0L);
            nota.setEmpresa(BuildConfig.EMPRESA);
            nota.setDataReferencia(Util.getFormatedDateLong(nota.getDataEnvio()));
            nota.setNotaNome("");
            nota.setNotaCNPJ("");
            nota.setNotaData("");
            nota.setNotaDescricao("");
            nota.save();




            user = new User();
            user.setUserLocal(userLocal);
            Gson gson = new Gson();
            Log.d("ENVIOREST",gson.toJson(user));

            Log.d("ENVIOREST","USER ---->"+ userLocal.toString());
            Log.d("ENVIOREST","ENVIO---->"+ user.toString());

            showProgress();


        } catch (IOException e) {
            Log.e("ERROBITMAP",e.getMessage());
        }


        File file = saveImage(bitmap,nota.getArquivo(), uri);

        Log.d("UPLOAD","File size:"+Util.getFileSize(file));

        uploadMultipart(this,file,nota.getId());


    }


    @Override
    public void onImageCropped(Uri uri, File imageFile) {

//        try {
//
//            imagem1.setImageBitmap(Util.handleSamplingAndRotationBitmap(this,uri));
//
//            long currentTime = System.currentTimeMillis();
//
//            Nota nota = new Nota();
//            nota.setArquivo(uri.toString());
//            nota.setDataEnvio(Util.getTodayDate());
//            nota.setReferencia(userLocal.getCpf()+currentTime);
//            nota.setStatus(0);
//            nota.setUserId(userLocal.getCpf());
//            nota.setValor(0.0d);
//            nota.save();
//
//            user = new User();
//            user.setUserLocal(userLocal);
//            user.setNotas(Nota.findWithQuery(Nota.class,"select * from nota where status < 2",null));
//
//            mProgressBar.setVisibility(View.VISIBLE);
//
//            Log.d("ENVIOREST","AQUI AQUI-->"+user.toString());
//
//            new EnviaNotas().execute(user);
//
//
//        } catch (IOException e) {
//            Log.e("ERROBITMAP",e.getMessage());
//        }

    }

    public Bitmap doFilter(Bitmap bitmap){

        ImageProcessor imageProcessor = new ImageProcessor();

        return imageProcessor.doGreyScale(bitmap);

    }

    public void uploadMultipart(final Context context, File file, final long notaId) {
        try {


            Log.d("LOGUPLOAD","Path:"+file.getPath());

            //http://192.168.0.248/nfpsolidaria/upload
            String uploadId =
                    new MultipartUploadRequest(context, BuildConfig.BASEURL+"nfpsolidaria/upload")
                            // starting from 3.1+, you can also use content:// URI string instead of absolute file
                            .addFileToUpload(file.getPath(), "file")
                            .setNotificationConfig(getNotificationConfig(notaId,"Upload de Cupom"))
                            .setMaxRetries(10)
                            .setDelegate(new UploadStatusDelegate() {
                                @Override
                                public void onProgress(Context context, UploadInfo uploadInfo) {
                                    Log.d("LOGUPLOAD",""+uploadInfo.getProgressPercent()+"%");
                                }

                                @Override
                                public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse,
                                                    Exception exception) {
                                    //Log.d("LOGUPLOAD","Erro:"+serverResponse.getBodyAsString());
                                    Snackbar.make(coordinatorLayout1, "Falha no Envio! Nova tentativa mais tarde", Snackbar.LENGTH_LONG)
                                            .setAction("Action", null).show();

                                }

                                @Override
                                public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {
                                    // your code here
                                    // if you have mapped your server response to a POJO, you can easily get it:
                                    // YourClass obj = new Gson().fromJson(serverResponse.getBodyAsString(), YourClass.class);
                                    Log.d("LOGUPLOAD","UPLOAD COMPLETO");

                                    Nota nota = Nota.findById(Nota.class,notaId);
                                    if(nota != null){
                                        if(nota.getStatus()==99){
                                            nota.setStatus(1);
                                            deleteNota(nota.getArquivo());
                                            nota.save();
                                            user.setNotas(Nota.findWithQuery(Nota.class,"select * from nota where status < 2",null));
                                            new EnviaNotas().execute(user);
                                        }
                                    }

                                }

                                @Override
                                public void onCancelled(Context context, UploadInfo uploadInfo) {
                                    // your code here
                                    Log.d("LOGUPLOAD","cancelado");
                                }
                            })
                            .startUpload();


            Log.d("LOGUPLOAD","--->"+uploadId);
        } catch (Exception exc) {
            Log.e("LOGUPLOAD", exc.getMessage(), exc);
        }
    }


    private class EnviaNotas extends AsyncTask<User, Void, String> {

        @Override
        protected String doInBackground(User... params) {

            Gson g = new Gson();

            Log.d("ENVIOREST","NOS PARAMENTROS-->"+g.toJson(params[0]));



            // Eventos
            Call<List<Nota>> call = new RestAPIClient().getRestService().getNotas(params[0]);
            call.enqueue(new Callback<List<Nota>>() {
                @Override
                public void onResponse(Call<List<Nota>> call, Response<List<Nota>> response) {

                    Log.d("ENVIOREST","User-->"+response.code());

                if(response.code()==200){
                    List<Nota> nn = response.body();
                    Nota nota;
                    for(Nota n: nn){
                        nn = Nota.findWithQuery(Nota.class,"select * from nota where referencia=?",n.getReferencia());
                        if(nn.size()>0){
                            nota = nn.get(0);
                            if(nota.getStatus()!=2) {
                                nota.setValor(n.getValor());
                                nota.setStatus(n.getStatus());
                                nota.setDataCredito(n.getDataCredito());
                                nota.setNotaNome(n.getNotaNome());
                                nota.setNotaCNPJ(n.getNotaCNPJ());
                                nota.setNotaData(n.getNotaData());
                                nota.setNotaDescricao(n.getNotaDescricao());
                                nota.save();
                            }
                        }
                    }
                }else{
                    Log.d("ENVIOREST","Erro de user:"+response.errorBody().toString());
                    hideProgress();
                }


                }
                @Override
                public void onFailure(Call<List<Nota>> call, Throwable t) {
                    Log.e("ENVIOREST",t.getMessage());
                    hideProgress();
                }
            });

            return "executed";
        }

        @Override
        protected void onPostExecute(String result) {

            hideProgress();
            Snackbar.make(coordinatorLayout1, "Cupom processado!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        }

    }


    private File saveImage(Bitmap finalBitmap, String fname, Uri uri) {

        // deleta arquivo no media manager
        File file2=new File(uri.getPath());
        if(file2.exists()){
            file2.delete();
        }

        String root;

        if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        }else{
            root = Environment.getExternalStorageDirectory().toString();
        }

        File myDir = new File(root + "/nfpsolidaria");
        myDir.mkdirs();

        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            return file;
        } catch (Exception e) {
           Log.e("LOGUPLOAD","ERROSALVAARQUIVO->"+e.getMessage());
           return null;
        }


    }

    private void deleteNota(String fname){
        String root;

        if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        }else{
            root = Environment.getExternalStorageDirectory().toString();
        }

        File myDir = new File(root + "/nfpsolidaria");
        myDir.mkdirs();

        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();


    }

    protected UploadNotificationConfig getNotificationConfig(final long uploadId, String title) {
        UploadNotificationConfig config = new UploadNotificationConfig();

        PendingIntent clickIntent = PendingIntent.getActivity(
                this, 1, new Intent(this, NotaActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

        config.setNotificationChannelId(""+uploadId);

        config.setTitleForAllStatuses(title)
                .setRingToneEnabled(true)
                .setClickIntentForAllStatuses(clickIntent)
                .setClearOnActionForAllStatuses(true);

        config.getProgress().message = "Fazendo upload do cupom...";
        config.getProgress().iconResourceID = R.drawable.ic_upload;
        config.getProgress().iconColorResourceID = Color.BLUE;

        config.getCompleted().message = "Upload feito com sucesso!";
        config.getCompleted().iconResourceID = R.drawable.ic_upload_success;
        config.getCompleted().iconColorResourceID = Color.GREEN;

        config.getError().message = "Erro no upload!";
        config.getError().iconResourceID = R.drawable.ic_upload_error;
        config.getError().iconColorResourceID = Color.RED;

        config.getCancelled().message = "Upload Cancelado!";
        config.getCancelled().iconResourceID = R.drawable.ic_cancelled;
        config.getCancelled().iconColorResourceID = Color.YELLOW;

        return config;
    }


}



