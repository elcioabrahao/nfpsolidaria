package nfpsolidaria.com.br.nfpsolidaria.ui.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;

import butterknife.BindView;
import butterknife.ButterKnife;
import nfpsolidaria.com.br.nfpsolidaria.R;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.Nota;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.StatusNota;
import nfpsolidaria.com.br.nfpsolidaria.domain.util.Util;

public class DetalhesNotaActivity extends AppCompatActivity {


    @BindView(R.id.textViewDataEnvio)
    TextView textViewDataEnvio;
    @BindView(R.id.textViewDataCredito)
    TextView textViewDataCredito;
    @BindView(R.id.textViewValor)
    TextView textViewValor;
    @BindView(R.id.textViewNotaNome)
    TextView textViewNotaNome;
    @BindView(R.id.textViewCNPJ)
    TextView textViewCNPJ;
    @BindView(R.id.textViewNotaData)
    TextView textViewNotaData;
    @BindView(R.id.textViewNotaDescricao)
    TextView textViewNotaDescricao;
    @BindView(R.id.textViewStatus)
    TextView textViewStatus;
    @BindView(R.id.textViewValorDoacao)
    TextView textViewValorDoacao;
    @BindView(R.id.textViewNotaNumero)
    TextView textViewNotaNumero;

    Nota nota;
    StatusNota sn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_nota);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        long idNota = intent.getLongExtra(NotaActivity.NOTAID,0L);

        nota = Nota.findById(Nota.class,idNota);
        if(nota !=null){

            textViewDataEnvio.setText(nota.getDataEnvio());
            textViewDataCredito.setText(nota.getDataCredito());
            textViewValor.setText(Util.formatCurrency(""+nota.getValor()));
            textViewValorDoacao.setText(Util.formatCurrency(""+nota.getValorDoado()));
            textViewNotaNumero.setText(""+nota.getNotaNumero());
            textViewNotaNome.setText(nota.getNotaNome());
            textViewCNPJ.setText(nota.getNotaCNPJ());
            textViewNotaDescricao.setText(nota.getNotaDescricao());
            sn = StatusNota.findById(StatusNota.class, Long.parseLong(""+nota.getStatus()));
            textViewStatus.setText(sn.getDescricao());


        }else{

            finish();
        }


    }

    public void helpStatus(View view){
        new MaterialStyledDialog.Builder(this)
                .setTitle(sn.getDescricao())
                .setDescription(sn.getMensagem())
                //.setHeaderDrawable(R.drawable.header)
                .setStyle(Style.HEADER_WITH_ICON)
                .withDarkerOverlay(false)
                .setIcon(R.drawable.icon_help_128)
                //.setHeaderDrawable(R.drawable.logo_cadastro_reduzido)
                .withDialogAnimation(true)
                .setScrollable(true)
                .setCancelable(true)
                .setPositiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                })
                .show();
    }

    public void fechaActivity(View view){
        finish();
    }
}
