package nfpsolidaria.com.br.nfpsolidaria.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import nfpsolidaria.com.br.nfpsolidaria.R;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.Nota;
import nfpsolidaria.com.br.nfpsolidaria.domain.util.Util;
import nfpsolidaria.com.br.nfpsolidaria.ui.fragments.NotaFragment.OnListFragmentInteractionListener;

import java.util.List;


public class MyNotaRecyclerViewAdapter extends RecyclerView.Adapter<MyNotaRecyclerViewAdapter.ViewHolder> {

    private final List<Nota> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyNotaRecyclerViewAdapter(List<Nota> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_nota, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mDataEnvioView.setText("Data envio: "+mValues.get(position).getDataEnvio());
        String dataCredito = "";
        switch (mValues.get(position).getStatus()){
            case 99:
                holder.mStatusView.setImageResource(R.drawable.ic_file_upload_white_24dp);
                //holder.mStatusView.setImageResource(R.drawable.ic_done_white_24dp);
                dataCredito="Aguardando Upload de Foto";
                break;
            case 1:
                //holder.mStatusView.setImageResource(R.drawable.ic_file_upload_white_24dp);
                holder.mStatusView.setImageResource(R.drawable.ic_done_white_24dp);
                dataCredito="Aguardando Processamento";
                break;
            case 2:
                holder.mStatusView.setImageResource(R.drawable.ic_done_all_white_24dp);
                holder.mValorView.setText(Util.formatCurrency(""+mValues.get(position).getValor()));
                dataCredito="Data crédito: "+mValues.get(position).getDataCredito();
                break;
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                holder.mStatusView.setImageResource(R.drawable.ic_error_outline_white_24dp);
                dataCredito="Data recusa: "+mValues.get(position).getDataCredito();
                break;
        }

        holder.mDataCreditoView.setText(dataCredito);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mDataEnvioView;
        public final ImageView mStatusView;
        public final TextView mDataCreditoView;
        public final TextView mValorView;

        public Nota mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mDataEnvioView = (TextView) view.findViewById(R.id.dataEnvio);
            mStatusView = (ImageView) view.findViewById(R.id.imageViewStatus);
            mDataCreditoView = (TextView) view.findViewById(R.id.dataCredito);
            mValorView = (TextView) view.findViewById(R.id.valorCredito);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mDataEnvioView.getText() + "'";
        }
    }

    public void updateList(List<Nota> newlist) {
        mValues.clear();
        mValues.addAll(newlist);
        this.notifyDataSetChanged();
    }
}
