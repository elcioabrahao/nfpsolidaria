package nfpsolidaria.com.br.nfpsolidaria.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nfpsolidaria.com.br.nfpsolidaria.R;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.Nota;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.ResumoMes;
import nfpsolidaria.com.br.nfpsolidaria.domain.util.Util;
import nfpsolidaria.com.br.nfpsolidaria.ui.adapters.MyExpandableListAdapter;


public class EstatisticaActivity extends AppCompatActivity {

    // Declare ExpandableListView
    ExpandableListView expandableListView;

    // Create ExpandableListAdapter
    ExpandableListAdapter expandableListAdapter;

    List<ResumoMes> parents; // A list of parents (strings)
    Map<ResumoMes, List<Nota>> childrenMap; // An object that maps keys to values.
    // A map cannot contain duplicate keys; each key can map to at most one value
    // Map Documentation: https://developer.android.com/reference/java/util/Map.html
    // HashMap Documentation: https://developer.android.com/reference/java/util/HashMap.html
    // Hashtable Documentation: https://developer.android.com/reference/java/util/Hashtable.html


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estatistica);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Get the expandablelistview from our XML layout file
        expandableListView = (ExpandableListView) findViewById(R.id.list_view_expandable);

        // Populate our data and provide associations
        fillData();

        /**
         * Instantiate our ExpandableListAdapter, pass through
         *  - Context (this)
         *  - List of Parents (parents)
         *  - Map populated with our values and their associations (childrenMap)
         *  (requires a constructor which accepts these values)
         */
        expandableListAdapter = new MyExpandableListAdapter(this, parents, childrenMap);

        // Set the value in the ExpandableListView
        expandableListView.setAdapter(expandableListAdapter);

        /** Set a listener on the child elements - show toast as an example */
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                Intent intent = new Intent(EstatisticaActivity.this,DetalhesNotaActivity.class);
                intent.putExtra(NotaActivity.NOTAID,childrenMap.get(parents.get(groupPosition)).get(childPosition).getId());
                startActivity(intent);
                return false;
            }
        });


    }

    /**
     * Populate our parents and children with values, associate children to parents with HashMap
     */
    public void fillData() {
        parents = new ArrayList<ResumoMes>(); // List of Parent Items
        childrenMap = new HashMap<ResumoMes,List<Nota>>(); // HashMap to map keys to values

        Map<String,String> mapa = new HashMap<String,String>();
        List<Nota> lista = Nota.findWithQuery(Nota.class,"SELECT * FROM nota WHERE status = 2 ORDER BY data_referencia");
        if(lista.size()>0){
            String data;
            for(Nota n:lista){
                data=Util.getFormatedMonthAndYear(n.getDataCredito());
                Log.d("ESTATISTICA","----->"+data);
                mapa.put(data,data);
            }
        }
        if(mapa.size()>0){
            Iterator it = mapa.values().iterator();

            String mesAno="";
            ResumoMes resumoMes=null;

            while (it.hasNext()) {

                resumoMes=new ResumoMes();
                mesAno=(String)it.next();
                Log.d("ESTATISTICA","MesAno-->"+mesAno);
                lista = Nota.findWithQuery(Nota.class,"SELECT * FROM nota WHERE status=2 AND data_referencia LIKE '"+Util.getUnformatedMontAndYear(mesAno)+"%' ORDER BY data_referencia");
                Log.d("ESTATISTICA","ListaSize-->"+lista.size());
                if(lista.size()>0){
                    for(Nota nnn:lista){
                        Log.d("ESTATISTICA","Cupom-->"+nnn.getValor());
                        resumoMes.addValorCupom(nnn.getValor());
                        resumoMes.addValorDoado(nnn.getValorDoado());
                    }
                    resumoMes.setMesAno(mesAno);
                    childrenMap.put(resumoMes,lista);
                }
                parents.add(resumoMes);

            }

        }



    }



}
