package nfpsolidaria.com.br.nfpsolidaria.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nfpsolidaria.com.br.nfpsolidaria.BuildConfig;
import nfpsolidaria.com.br.nfpsolidaria.R;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.UserLocal;
import nfpsolidaria.com.br.nfpsolidaria.domain.util.CpfCnpjMasks;
import nfpsolidaria.com.br.nfpsolidaria.domain.util.DateMask;
import nfpsolidaria.com.br.nfpsolidaria.domain.util.Encryptor;
import nfpsolidaria.com.br.nfpsolidaria.domain.util.Util;


public class CadastroActivity extends AppCompatActivity {

    private UserLocal userLocal;

    @BindView(R.id.editTextNome)
    public EditText editTextNome;
    @BindView(R.id.editTextCPF)
    public EditText editTextCPF;
    @BindView(R.id.editTextDataNascimento)
    public EditText editTextDataNascimento;
    @BindView(R.id.editTextSenha)
    public EditText editTextSenhaSEFAZ;
    @BindView(R.id.editTextConfirmaSenha)
    public EditText editTextConfirmaSenhaSEFAZ;


    private String nome;
    private String cpf;
    private String dataNascimento;
    private String senhaSefaz;
    private String confirmaSenhaSefaz;
    private Intent intent;
    private boolean showDisclaimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);


        ButterKnife.bind(this);

        intent = getIntent();
        int cadastro = intent.getIntExtra(MainActivity.CADASTRO,1);

        Log.d("Cadastro","Cadastro: "+cadastro);

        if(cadastro==MainActivity.NOVO_REGISTRO) {

            showDisclaimer = true;
            userLocal = new UserLocal();

            new MaterialStyledDialog.Builder(this)
                    .setTitle(R.string.tituloPoupUp)
                    .setDescription(R.string.mensagemPoupUp)
                    //.setHeaderDrawable(R.drawable.header)
                    .setStyle(Style.HEADER_WITH_ICON)
                    .withDarkerOverlay(false)
                    //.setIcon(R.drawable.logo_grande_principal)
                    .setHeaderDrawable(R.drawable.logo_cadastro_reduzido)
                    .withDialogAnimation(true)
                    .setCancelable(true)
                    .setPositiveText("OK")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .show();
        }else{

            showDisclaimer = false;
            userLocal = UserLocal.findById(UserLocal.class,1);
            Log.d("Cadastro","Chegou aqui...");
            editTextNome.setText(userLocal.getNome());
            editTextDataNascimento.setText(userLocal.getDataNascimento());
            editTextCPF.setText(userLocal.getCpf());
            String senha = Encryptor.decrypt(BuildConfig.WEBSERVICEPASS,BuildConfig.RANDOMINITIVECTOR,userLocal.getSenhaSEFAZ());
            editTextSenhaSEFAZ.setText(senha);
            editTextConfirmaSenhaSEFAZ.setText(senha);


        }

        editTextCPF.addTextChangedListener(CpfCnpjMasks.insert(editTextCPF));
        editTextDataNascimento.addTextChangedListener(DateMask.insert(editTextDataNascimento));

    }

    @OnClick(R.id.buttonSalvar)
    public void onClickSave(){

        if(validateForm()){

            if(showDisclaimer){
                new MaterialStyledDialog.Builder(this)
                        .setTitle(R.string.termoTitulo)
                        .setDescription(R.string.termoMensagem)
                        //.setHeaderDrawable(R.drawable.header)
                        .setStyle(Style.HEADER_WITH_ICON)
                        .withDarkerOverlay(false)
                        .setIcon(R.drawable.atention)
                        //.setHeaderDrawable(R.drawable.logo_cadastro_reduzido)
                        .withDialogAnimation(true)
                        .setCancelable(true)
                        .setScrollable(true)
                        .setPositiveText("SIM")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                userLocal.setNome(nome);
                                userLocal.setCpf(cpf);
                                userLocal.setDataNascimento(dataNascimento);
                                userLocal.setSenhaSEFAZ(Encryptor.encrypt(BuildConfig.WEBSERVICEPASS,BuildConfig.RANDOMINITIVECTOR,senhaSefaz));
                                userLocal.save();
                                setResult(MainActivity.RESULTADO_CADASTRO_SUCESSO);
                                finish();
                            }
                        })
                        .setNegativeText("NÃO")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                setResult(MainActivity.RESULTADO_CADASTRO_CANCELADO);
                                finish();
                            }
                        })
                        .show();
            }else{
                userLocal.setNome(nome);
                userLocal.setCpf(cpf);
                userLocal.setDataNascimento(dataNascimento);
                userLocal.setSenhaSEFAZ(Encryptor.encrypt(BuildConfig.WEBSERVICEPASS,BuildConfig.RANDOMINITIVECTOR,senhaSefaz));
                userLocal.save();
                setResult(MainActivity.RESULTADO_CADASTRO_SUCESSO);
                finish();
            }


        }

    }

    @OnClick(R.id.buttonCancelar)
    public void onClickCancel(){
        setResult(MainActivity.RESULTADO_CADASTRO_CANCELADO);
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(MainActivity.RESULTADO_CADASTRO_CANCELADO);
        finish();
    }

    private boolean validateForm(){

        boolean valid = true;

        nome = editTextNome.getText().toString();
        cpf = editTextCPF.getText().toString();
        dataNascimento = editTextDataNascimento.getText().toString();
        senhaSefaz = editTextSenhaSEFAZ.getText().toString();
        confirmaSenhaSefaz=editTextConfirmaSenhaSEFAZ.getText().toString();

        editTextNome.setError(null);
        editTextCPF.setError(null);
        editTextDataNascimento.setError(null);
        editTextSenhaSEFAZ.setError(null);
        editTextConfirmaSenhaSEFAZ.setError(null);

        if(nome.isEmpty()){
            editTextNome.setError("nome não pode estar vazio");
            valid = false;
        } else
        if(nome.length()<=3){
            editTextNome.setError("nome deve ter mais de 3 caracteres");
            valid = false;
        } else
        if(nome.indexOf(" ")==-1){
            editTextNome.setError("por favor forneça nome completo");
            valid = false;
        } else
        if(cpf.isEmpty()){
            editTextCPF.setError("cpf não pode estar vazio");
            valid = false;
        } else
        if(cpf.length()!=14){
            editTextCPF.setError("cpf deve ter 11 números");
            valid = false;
        } else
        if(!Util.isCPF(cpf)){
            editTextCPF.setError("cpf inválido");
            valid = false;
        } else
        if(dataNascimento.isEmpty()){
            editTextDataNascimento.setError("data de nascimento não pode estar vazia");
            valid = false;
        } else
        if(editTextDataNascimento.length()!=10){
            editTextDataNascimento.setError("data de nascimento deve estar no formato dd/mm/aaaa");
            valid = false;
        }
        if(!Util.maiorQueDezesseis(editTextDataNascimento.getText().toString())){
            editTextDataNascimento.setError("para fazer uma doação você precisa ser maior de 16 anos");
            valid = false;
        }else
        if(!Util.isDateValidBr(dataNascimento)){
            editTextDataNascimento.setError("data de nascimento inválida");
            valid = false;
        } else
        if(senhaSefaz.isEmpty()){
            editTextSenhaSEFAZ.setError("senha não pode estar vazio");
            valid = false;
        } else
        if(confirmaSenhaSefaz.isEmpty()){
            editTextSenhaSEFAZ.setError("confirma senha não pode estar vazio");
            valid = false;
        } else
        if(!senhaSefaz.equals(confirmaSenhaSefaz)){
            editTextConfirmaSenhaSEFAZ.setError("senha e confirmação não são as mesmas");
            valid = false;
        }

        return valid;

    }

}
