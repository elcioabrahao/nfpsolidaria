package nfpsolidaria.com.br.nfpsolidaria.domain.model;

/**
 * Created by elcio on 27/12/17.
 */

public class ResumoMes {

    private String mesAno;
    private int numeroDoacoes;
    private long valorCupons;
    private long valorDoado;

    public ResumoMes() {
        numeroDoacoes=0;
        valorCupons=0L;
        valorDoado=0L;
    }

    public String getMesAno() {
        return mesAno;
    }

    public void setMesAno(String mesAno) {
        this.mesAno = mesAno;
    }

    public int getNumeroDoacoes() {
        return numeroDoacoes;
    }

    public void setNumeroDoacoes(int numeroDoacoes) {
        this.numeroDoacoes = numeroDoacoes;
    }

    public long getValorCupons() {
        return valorCupons;
    }

    public void setValorCupons(long valorCupons) {
        this.valorCupons = valorCupons;
    }

    public long getValorDoado() {
        return valorDoado;
    }

    public void setValorDoado(long valorDoado) {
        this.valorDoado = valorDoado;
    }

    public void addValorCupom(long valor){
        valorCupons+=valor;
        numeroDoacoes++;
    }

    public void addValorDoado(long valor){
        valorDoado+=valor;
    }


}
