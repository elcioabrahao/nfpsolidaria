package nfpsolidaria.com.br.nfpsolidaria.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by elcio on 07/11/17.
 */


public class Teste implements Serializable {

    @Expose
    int id;
    @Expose
    String nome;
    @Expose
    double salario;

    @Expose
    List<Teste> teste;

    public List<Teste> getTeste() {
        return teste;
    }

    public void setTeste(List<Teste> teste) {
        this.teste = teste;
    }

    public Teste(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
}
