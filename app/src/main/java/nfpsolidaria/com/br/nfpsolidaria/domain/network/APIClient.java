package nfpsolidaria.com.br.nfpsolidaria.domain.network;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;

import retrofit2.Retrofit;

import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class APIClient {

    private static Retrofit REST_ADAPTER;

    //.setEndpoint("http://10.0.3.2:8080/pdvserver/rest")

    private static void createAdapterIfNeeded() {

        if (REST_ADAPTER == null) {

            final OkHttpClient okHttpClient = new OkHttpClient();


            // http://intra.lliege.com.br/ci/sist_portal_servicos_publicos/IntegracaoPSP/
            // http://intra.lliege.com.br/ci/sist_portal_servicos_publicos/IntegracaoPSP/iluminacaoPublica

            REST_ADAPTER = new Retrofit.Builder()
                    .baseUrl("http://URLAQUI")
                    .addConverterFactory(GsonConverterFactory.create(getGson()))
                    .build();
        }
    }

    private static Gson getGson() {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    public APIClient() {
        createAdapterIfNeeded();
    }

    public RestServices getRestService() {
        return REST_ADAPTER.create(RestServices.class);
    }


    public interface RestServices {


//        @GET("/produto/todos")
//        void getAllProdutos(
//                Callback<List<Produto>> callbackProdutos
//        );
//
//        @Headers( "Content-Type: application/json" )
//        @POST("/compra/cadastro")
//        void enviarCompra(
//                @Body Compra compra,
//                Callback<String> callbackCompra
//        );
//

//
//        @FormUrlEncoded()
//        @PUT("/produto")
//        void updateProduto(
//                @Field("id") String codigoBarras,
//                @Field("descricao") String descricao,
//                @Field("unidade") String unidade,
//                @Field("preco") double preco,
//                @Field("foto") String foto,
//                @Field("ativo") int ativo,
//                @Field("latitude") double latitude,
//                @Field("longitude") double longitude,
//                Callback<String> callbackUpdateProduto
//        );
//
//
//        @DELETE("/produto")
//        String deleteProduto(
//                @Query("id") String codigoBarras
//        );
//
//        //@Headers( "Content-Type: application/json" )
//        @POST("/usuario")
//        void login(
//                @Body Login login,
//                Callback<RestMessage> callbackLogin
//        );
//
//        @POST("/inserirOcorrencia")
//        void inserirOcorrencia(
//                @Body Ocorrencia Ocorrencia,
//                Callback<RestMessage> callbackLogin
//        );
//
//        @GET("/buscarOcorrencia")
//        void getMinhasOcorrencias(
//                @Query("remoteId") long remoteId,
//                Callback<OcorrenciaRest> callbackOcorrencias
//        );
//
//        @GET("/buscarOcorrencia")
//        void getTodasOcorrencias(
//                Callback<OcorrenciaRest> callbackOcorrencias
//        );




//        @GET("/os")
//        void getAllOrdemServico(@Header("Authorization") String token,
//                                Callback<List<Ocorrencia>> callbackOrdemServico
//        );
//
//
//        @PUT("/os/update")
//        void sendOSToServer(
//                @Header("Authorization") String token,
//                @Body Ocorrencia ocorrencia,
//                Callback<RestMessage> callbackOrdemServico
//        );


//        @GET("/iluminacaoPublica")
//        void getIluminationPoints(
//                @Query("tipo") int tipo,
//                @Query("latitude") double latitude,
//                @Query("longitude") double longitude,
//                @Query("raio") double raio,
//                Callback<RestMessageIluminationPoint> callbackIluminationPoints
//        );
//
//
//        @POST("/registrarOcorrenciaWeb")
//        void registrarOcorrencia(
//                @Body Ocorrencia Ocorrencia,
//                Callback<RestMessage> callbackRegistrarOcorrencia
//        );
//
//
//        // remoteIdUsuario,remoteIdOrcamento,voto
//
//        @POST("/registraVotoOrcamentoParticipativo")
//        void setVoto(
//                @Body VotoOrcamento votoOrcamento,
//                Callback<Orcamento> callbackOrcamento
//        );
//
//
//        @GET("/buscarOrcamentoParticipativo")
//        void getOrcamentos(
//                @Query("remoteIdUsuario") long remoteId,
//                Callback<OrcamentoResponse> callbackOrcamentos
//        );


    }


}