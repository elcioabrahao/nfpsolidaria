package nfpsolidaria.com.br.nfpsolidaria.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by elcio on 30/10/17.
 */

public class RestMessageNotas implements Serializable{

    public RestMessageNotas(){}

    @Expose
    private Nota[] notas;

    public Nota[] getNotas() {
        return notas;
    }

    public void setNotas(Nota[] notas) {
        this.notas = notas;
    }
}
