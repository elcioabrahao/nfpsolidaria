package nfpsolidaria.com.br.nfpsolidaria.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import nfpsolidaria.com.br.nfpsolidaria.R;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.Nota;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.ResumoMes;
import nfpsolidaria.com.br.nfpsolidaria.domain.util.Util;

/**
 * Created by mattr on 7/5/2017.
 */

public class MyExpandableListAdapter extends BaseExpandableListAdapter {

    // Declare context object
    private Context mContext;
    private List<ResumoMes> mParents;
    private Map<ResumoMes, List<Nota>> mChildrenMap;

    /**
     * Constructor
     * @param context
     * @param parents - List of Parents (parents)
     * @param childrenMap - Map populated with our values and their associations (childrenMap)
     */
    public MyExpandableListAdapter(Context context, List<ResumoMes> parents, Map<ResumoMes, List<Nota>> childrenMap) {
        this.mContext = context;
        this.mParents = parents;
        this.mChildrenMap = childrenMap;
    }

    /**
     * @return - the number of "parents" or groups
     */
    @Override
    public int getGroupCount() {
        return mParents.size();
    }

    /**
     * @param groupPosition - the position we are in the map, each map position may have a different count
     * @return - the number of children in each group (under each parent)
     */
    @Override
    public int getChildrenCount(int groupPosition) {
        return mChildrenMap.get(mParents.get(groupPosition)).size();
    }

    /**
     * @param groupPosition
     * @return - for any specific position, return the group name
     */
    @Override
    public Object getGroup(int groupPosition) {
        return mParents.get(groupPosition);
    }

    /**
     * @param groupPosition
     * @param childPosition
     * @return - the name of the child (cycles through each parent (or group), and each child of each parent
     */
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mChildrenMap.get(mParents.get(groupPosition)).get(childPosition);
    }

    /**
     * @param groupPosition
     * @return the groupPosition as the ID
     */
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    /**
     * @param groupPosition
     * @param childPosition
     * @return the childPosition as the ID
     */
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    // not touched
    @Override
    public boolean hasStableIds() {
        return false;
    }

    /**
     * Get the value of the group item (parent), apply to the TextView using LayoutInflater
     * @param groupPosition
     * @param isExpanded
     * @param convertView // recycles old views in Adapters to increase performance
     * @param parent
     * @return the actual value
     */
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        // Get the value of the parent item
        ResumoMes resumoMes = (ResumoMes)getGroup(groupPosition);

        // Create the convertView object if it is null
        if(convertView == null) {
            // Inflate the view in our list_parent.xml
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_parent, null);
        }

        // Update the TextView in our convertView (based on the list_parent.xml)
        TextView textViewMesAno = (TextView) convertView.findViewById(R.id.parent_mes_ano);
        TextView textViewNumeroDoacoes = (TextView) convertView.findViewById(R.id.parent_numero_doacoes);
        TextView textViewValorCupons = (TextView) convertView.findViewById(R.id.parent_valor_cupons);
        TextView textViewValorDoacoes = (TextView) convertView.findViewById(R.id.parent_valor_doacoes);
        ImageView ivGroupIndicator = (ImageView) convertView.findViewById(R.id.ivGroupIndicator);

        textViewMesAno.setText(resumoMes.getMesAno());
        textViewNumeroDoacoes.setText(""+resumoMes.getNumeroDoacoes());
        textViewValorCupons.setText(Util.formatCurrency(""+resumoMes.getValorCupons()));
        textViewValorDoacoes.setText(Util.formatCurrency(""+resumoMes.getValorDoado()));
        ivGroupIndicator.setSelected(isExpanded);
        return convertView;
    }

    /**
     * Update and return the value of convertView
     * @param groupPosition
     * @param childPosition
     * @param isLastChild
     * @param convertView // recycles old views in Adapters to increase performance
     * @param parent
     * @return
     */
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        // get the value of the child item, in the parent (group)
        Nota nota = (Nota) getChild(groupPosition, childPosition);

        // Create the convertView object if it is null
        if(convertView == null) {
            // Inflate the view in our list_parent.xml
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_child, null);
        }

        // Update the TextView in our convertView (based on the list_child.xml)
        TextView textViewDataCredito = (TextView) convertView.findViewById(R.id.child_data_credito);
        TextView textViewNomeEmpresa = (TextView) convertView.findViewById(R.id.child_nome_empresa);
        TextView textViewValorCupom = (TextView) convertView.findViewById(R.id.child_valor_cupom);
        TextView textViewValorDoado = (TextView) convertView.findViewById(R.id.child_valor_doado);

        textViewDataCredito.setText(Util.getFormatedDateShort2(nota.getDataCredito()));
        textViewNomeEmpresa.setText(nota.getNotaNome());
        textViewValorCupom.setText(Util.formatCurrency(""+nota.getValor()));
        textViewValorDoado.setText(Util.formatCurrency(""+nota.getValorDoado()));


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true; // true - we want the child element to be selectable
    }
}
