package nfpsolidaria.com.br.nfpsolidaria.domain.network;


import java.util.List;

import nfpsolidaria.com.br.nfpsolidaria.domain.model.Nota;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.Teste;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.User;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.RestMessageNotas;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by elcio on 20/08/17.
 */

public interface RestAPI {


//        @GET("/produto/todos")
//        void getAllProdutos(
//                Callback<List<Produto>> callbackProdutos
//        );
//
//        @Headers( "Content-Type: application/json" )
//        @POST("/compra/cadastro")
//        void enviarCompra(
//                @Body Compra compra,
//                Callback<String> callbackCompra
//        );
//

//
//        @FormUrlEncoded()
//        @PUT("/produto")
//        void updateProduto(
//                @Field("id") String codigoBarras,
//                @Field("descricao") String descricao,
//                @Field("unidade") String unidade,
//                @Field("preco") double preco,
//                @Field("foto") String foto,
//                @Field("ativo") int ativo,
//                @Field("latitude") double latitude,
//                @Field("longitude") double longitude,
//                Callback<String> callbackUpdateProduto
//        );
//
//
//        @DELETE("/produto")
//        String deleteProduto(
//                @Query("id") String codigoBarras
//        );

    //@Headers( "Content-Type: application/json" )
//    @POST("ci/sist_portal_servicos_publicos/IntegracaoPSP/usuario")
//    Call<RestMessage> login(
//            @Body Login login
//    );
//
//    @POST("ci/sist_portal_servicos_publicos/IntegracaoPSP/inserirOcorrencia")
//    Call inserirOcorrencia(
//            @Body Ocorrencia Ocorrencia
//    );

//    @GET("ci/sist_portal_servicos_publicos/IntegracaoPSP/buscarOcorrencia")
//    Call getMinhasOcorrencias(
//            @Query("remoteId") long remoteId
//    );
//
//    @GET("ci/sist_portal_servicos_publicos/IntegracaoPSP/buscarOcorrencia")
//    Call getTodasOcorrencias(
//    );


    @POST("nfpsolidaria/rest/envio")
    Call<List<Nota>> getNotas(
            @Body()User user
    );

    @POST("nfpsolidaria/rest/teste")
    Call<Teste> getTeste(
            @Body()Teste teste

    );

    @GET("nfpsolidaria/rest/notas/empresa/{id}/cpf/{cpf}")
    Call<List<Nota>> getNotasByEmpresaCPF(@Path("id") int id, @Path("cpf") String cpf);


//        @GET("/os")
//        void getAllOrdemServico(@Header("Authorization") String token,
//                                Callback<List<Ocorrencia>> callbackOrdemServico
//        );
//
//
//        @PUT("/os/update")
//        void sendOSToServer(
//                @Header("Authorization") String token,
//                @Body Ocorrencia ocorrencia,
//                Callback<RestMessage> callbackOrdemServico
//        );

//
//    @GET("ci/sist_portal_servicos_publicos/IntegracaoPSP/iluminacaoPublica")
//    Call<RestMessageIluminationPoint> getIluminationPoints(
//            @Query("tipo") int tipo,
//            @Query("latitude") double latitude,
//            @Query("longitude") double longitude,
//            @Query("raio") double raio
//    );
//
//
//    @POST("ci/sist_portal_servicos_publicos/IntegracaoPSP/registrarOcorrenciaWeb")
//    Call<RestMessageReistrarOcorreciaIluminacao> registrarOcorrencia(
//            @Body Ocorrencia Ocorrencia
//    );
//
//
//    // remoteIdUsuario,remoteIdOrcamento,voto
//
//    @POST("ci/sist_portal_servicos_publicos/IntegracaoPSP/registraVotoOrcamentoParticipativo")
//    Call setVoto(
//            @Body VotoOrcamento votoOrcamento
//    );
//
//
//    @GET("ci/sist_portal_servicos_publicos/IntegracaoPSP/buscarOrcamentoParticipativo")
//    Call getOrcamentos(
//            @Query("remoteIdUsuario") long remoteId
//    );
//
//
//    @GET("ci/sist_agendamento/api/classificacao")
//    Call<RestMessageClassificacoes> getClassificacoes();
//
//    @GET("ci/sist_agendamento/api/publico")
//    Call<RestMessagePublicosAlvo> getPublicosAlvo();
//
//    @GET("ci/sist_agendamento/api/tipos/eventos")
//    Call<RestMessageTiposEvento> getTiposEventos();
//
//    @GET("ci/sist_agendamento/api/tipos/locais")
//    Call<RestMessageTiposLocal> getTiposLocais();
//
//    @GET("ci/sist_agendamento/api/organizadores")
//    Call<RestMessageOrganizadores> getOrganizadores();
//
//    @GET("ci/sist_agendamento/api/locais")
//    Call<RestMessageLocalsEventos> getLocais();
//
//    @GET("ci/sist_agendamento/api/eventos")
//    Call<RestMessageEventos> getEventos();







}
