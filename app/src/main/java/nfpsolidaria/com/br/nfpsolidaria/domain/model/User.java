package nfpsolidaria.com.br.nfpsolidaria.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by elcio on 30/10/17.
 */

public class User implements Serializable {


    @Expose
    private int id;
    @Expose
    private String nome;
    @Expose
    private String cpf;
    @Expose
    private String dataNascimento;
    @Expose
    private String senhaSEFAZ;
    @Expose
    public List<Nota> notas;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSenhaSEFAZ() {
        return senhaSEFAZ;
    }

    public void setSenhaSEFAZ(String senhaSEFAZ) {
        this.senhaSEFAZ = senhaSEFAZ;
    }

    public List<Nota> getNotas() {
        return notas;
    }

    public void setNotas(List<Nota> notas) {
        this.notas = notas;
    }

    @Override
    public String toString() {
        return "Envio{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                ", dataNascimento='" + dataNascimento + '\'' +
                ", senhaSEFAZ='" + senhaSEFAZ + '\'' +
                '}';
    }

    public void setUserLocal(UserLocal u){
        setCpf(u.getCpf());
        setDataNascimento(u.getDataNascimento());
        setNome(u.getNome());
        setSenhaSEFAZ(u.getSenhaSEFAZ());
    }
}
