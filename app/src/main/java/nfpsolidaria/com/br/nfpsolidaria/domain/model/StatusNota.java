package nfpsolidaria.com.br.nfpsolidaria.domain.model;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by elcio on 26/12/17.
 */

public class StatusNota extends SugarRecord implements Serializable {

    private String descricao;
    private String mensagem;

    public StatusNota(long id, String descricao, String mensagem) {
        this.descricao = descricao;
        this.mensagem = mensagem;
        setId(id);
    }

    public StatusNota() {
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
