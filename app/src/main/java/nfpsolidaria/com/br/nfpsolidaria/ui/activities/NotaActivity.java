package nfpsolidaria.com.br.nfpsolidaria.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.bluehomestudio.progresswindow.ProgressWindow;
import com.bluehomestudio.progresswindow.ProgressWindowConfiguration;

import java.util.List;

import nfpsolidaria.com.br.nfpsolidaria.BuildConfig;
import nfpsolidaria.com.br.nfpsolidaria.R;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.Nota;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.User;
import nfpsolidaria.com.br.nfpsolidaria.domain.model.UserLocal;
import nfpsolidaria.com.br.nfpsolidaria.domain.network.RestAPIClient;
import nfpsolidaria.com.br.nfpsolidaria.ui.fragments.NotaFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NotaActivity extends AppCompatActivity implements NotaFragment.OnListFragmentInteractionListener {

    public static final String NOTAID="notaid";
    private ProgressWindow progressWindow;
    UserLocal user;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nota);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        progressConfigurations();

        user = UserLocal.findById(UserLocal.class,1L);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabnota);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showProgress();
                String cpf = user.getCpf().replace(".","").replace("-","");


                // Eventos
                Call<List<Nota>> call = new RestAPIClient().getRestService().getNotasByEmpresaCPF(Integer.parseInt(BuildConfig.EMPRESA),cpf);
                call.enqueue(new Callback<List<Nota>>() {
                    @Override
                    public void onResponse(Call<List<Nota>> call, Response<List<Nota>> response) {

                        Log.d("ENVIOREST","User-->"+response.code());

                        if(response.code()==200){
                            List<Nota> nn = response.body();
                            if(nn.size()==0){
                                Nota.deleteAll(Nota.class);
                            }
                            List<Nota> ln;
                            Nota n1;
                            int status;
                            for(Nota n: nn){
                                status = n.getStatus();
                                ln = Nota.findWithQuery(Nota.class,"select * from nota where referencia = '"+n.getReferencia()+"'");
                                if(ln.size()>0){
                                    n1 = ln.get(0);
                                    if(n1.getStatus()==1 && status==0){
                                        n.setStatus(1);
                                    }
                                    n.setId(n1.getId());
                                    n.save();
                                }else{
                                    n.setId(0L);
                                    n.save();
                                }

                            }
                            hideProgress();
                            notifyDataSetChange();
                        }else{
                            Log.d("ENVIOREST","Erro de user:"+response.errorBody().toString());
                            hideProgress();
                        }


                    }
                    @Override
                    public void onFailure(Call<List<Nota>> call, Throwable t) {
                        Log.e("ENVIOREST",t.getMessage());
                        hideProgress();
                    }
                });

            }
        });

    }

    private void progressConfigurations(){
        progressWindow = ProgressWindow.getInstance(this);
        ProgressWindowConfiguration progressWindowConfiguration = new ProgressWindowConfiguration();
        progressWindowConfiguration.backgroundColor = Color.parseColor("#32000000") ;
        progressWindowConfiguration.progressColor = getResources().getColor(R.color.colorPrimary);
        progressWindow.setConfiguration(progressWindowConfiguration);
    }

    public void showProgress(){
        progressWindow.showProgress();
    }


    public void hideProgress(){
        progressWindow.hideProgress();
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideProgress();
    }

    @Override
    public void onListFragmentInteraction(Nota item) {
        Intent intent = new Intent(this,DetalhesNotaActivity.class);
        intent.putExtra(NOTAID,item.getId());
        startActivity(intent);
    }

    private void notifyDataSetChange(){

        NotaFragment notaFrag = (NotaFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragmentNota);

        if (notaFrag != null) {
            notaFrag.notifyDataSetChange();
        }else{
            Log.d("NOTIFICACAO","Nullo !");
        }
    }
}
