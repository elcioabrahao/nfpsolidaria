package nfpsolidaria.com.br.nfpsolidaria.domain.model;

import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by elcio on 19/10/17.
 */

public class Nota extends SugarRecord implements Serializable {

    @Expose
    public String arquivo;
    @Expose
    public String referencia;
    @Expose
    public String dataEnvio;
    @Expose
    public String dataCredito;
    @Expose
    public String userId;
    @Expose
    public int status;
    @Expose
    public long valor;
    @Expose
    public String empresa;
    @Expose
    public String dataReferencia;
    @Expose
    public long notaNumero;
    @Expose
    public long valorDoado;
    @Expose
    public String notaNome;
    @Expose
    public String notaCNPJ;
    @Expose
    public String notaData;
    @Expose
    public String notaDescricao;


    public Nota(){}


    public Nota(String arquivo, String referencia, String dataEnvio, String dataCredito, String userId, int status, long valor, String empresa, String dataReferencia, long notaNumero, long valorDoado, String notaNome, String notaCNPJ, String notaData, String notaDescricao) {
        this.arquivo = arquivo;
        this.referencia = referencia;
        this.dataEnvio = dataEnvio;
        this.dataCredito = dataCredito;
        this.userId = userId;
        this.status = status;
        this.valor = valor;
        this.empresa = empresa;
        this.dataReferencia = dataReferencia;
        this.notaNumero = notaNumero;
        this.valorDoado = valorDoado;
        this.notaNome = notaNome;
        this.notaCNPJ = notaCNPJ;
        this.notaData = notaData;
        this.notaDescricao = notaDescricao;
    }

    public String getDataReferencia() {
        return dataReferencia;
    }

    public void setDataReferencia(String dataReferencia) {
        this.dataReferencia = dataReferencia;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getArquivo() {
        return arquivo;
    }

    public void setArquivo(String arquivo) {
        this.arquivo = arquivo;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(String dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public String getDataCredito() {
        return dataCredito;
    }

    public void setDataCredito(String dataCredito) {
        this.dataCredito = dataCredito;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getNotaNome() {
        return notaNome;
    }

    public void setNotaNome(String notaNome) {
        this.notaNome = notaNome;
    }

    public String getNotaCNPJ() {
        return notaCNPJ;
    }

    public void setNotaCNPJ(String notaCNPJ) {
        this.notaCNPJ = notaCNPJ;
    }

    public String getNotaData() {
        return notaData;
    }

    public void setNotaData(String notaData) {
        this.notaData = notaData;
    }

    public String getNotaDescricao() {
        return notaDescricao;
    }

    public void setNotaDescricao(String notaDescricao) {
        this.notaDescricao = notaDescricao;
    }

    public long getNotaNumero() {
        return notaNumero;
    }

    public void setNotaNumero(long notaNumero) {
        this.notaNumero = notaNumero;
    }

    public long getValorDoado() {
        return valorDoado;
    }

    public void setValorDoado(long valorDoado) {
        this.valorDoado = valorDoado;
    }

    public void setValor(long valor) {
        this.valor = valor;
    }

    public long getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return "Nota{" +
                "arquivo='" + arquivo + '\'' +
                ", referencia='" + referencia + '\'' +
                ", dataEnvio='" + dataEnvio + '\'' +
                ", dataCredito='" + dataCredito + '\'' +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", valor=" + valor +
                ", empresa='" + empresa + '\'' +
                ", dataReferencia='" + dataReferencia + '\'' +
                ", notaNumero=" + notaNumero +
                ", valorDoado=" + valorDoado +
                ", notaNome='" + notaNome + '\'' +
                ", notaCNPJ='" + notaCNPJ + '\'' +
                ", notaData='" + notaData + '\'' +
                ", notaDescricao='" + notaDescricao + '\'' +
                '}';
    }



}
