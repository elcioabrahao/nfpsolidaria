package nfpsolidaria.com.br.nfpsolidaria;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.StrictMode;
import android.util.Log;

import com.orm.SchemaGenerator;
import com.orm.SugarApp;
import com.orm.SugarContext;
import com.orm.SugarDb;

import net.gotev.uploadservice.Logger;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.okhttp.OkHttpStack;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;

import nfpsolidaria.com.br.nfpsolidaria.domain.model.StatusNota;
import nfpsolidaria.com.br.nfpsolidaria.domain.network.BasicAuthInterceptor;
import nfpsolidaria.com.br.nfpsolidaria.domain.util.Tls12SocketFactory;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by elcio on 05/10/17.
 */

public class NFPApplication extends SugarApp {
    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(getApplicationContext());

        // create table if not exists
        SchemaGenerator schemaGenerator = new SchemaGenerator(this);
        schemaGenerator.createDatabase(new SugarDb(this).getDB());
// Set your application namespace to avoid conflicts with other apps
        // using this library
        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;

        // Set upload service debug log messages level
        Logger.setLogLevel(Logger.LogLevel.DEBUG);

        // Set up the Http Stack to use. If you omit this or comment it, HurlStack will be
        // used by default
        //UploadService.HTTP_STACK = new OkHttpStack(getOkHttpClient());
        UploadService.HTTP_STACK = new OkHttpStack(getHttpClient(this));

        // setup backoff multiplier
        UploadService.BACKOFF_MULTIPLIER = 2;

        createStatus();

    }

    private void createStatus(){

        if(!StatusNota.findAll(StatusNota.class).hasNext()){
            StatusNota sn;
            sn = new StatusNota(1L,"Aguardando processamento","O upload da imagem do cupom foi feito com sucesso. A cupom será enviado a secretaria da fazenda e o resultado será atualizado em até 120 dias");
            sn.save();
            sn = new StatusNota(2L,"Doação confirmada","O cupom foi processado com sucesso e a doação já foi registrada na secretaria da fazenda");
            sn.save();
            sn = new StatusNota(3L,"A imagem do cupom esta ilegível","A foto do cupom não está legível o suficiente para que o cupom possa ser doado");
            sn.save();
            sn = new StatusNota(4L,"O cupom está identificado com CPF","Somente cupons e notas fiscais sem a identificação do CPF, por parte do consumidor no ato da compra, podem ser doados");
            sn.save();
            sn = new StatusNota(5L,"O usuário e/ou senha fornecido não corresponde ao utilizado no portal da secretaria da fazenda do estado de São Paulo","A senha e o CPF fornecidos previamente no cadastrado (pode ser um link para a parte do cadastro), devem ser a mesma utilizada para entrar no site do programa nota fiscal paulista, caso não tenha o cadastro, você pode realizá-lo clicando aqui (hyperlink para: https://www.nfp.fazenda.sp.gov.br/cadastroCAT/SolicitacaoCadastroCPF.aspx), caso tenha o cadastro mas não lembra a senha vá até o portal (hyperlink para: https://www.nfp.fazenda.sp.gov.br/login.aspx) e clique em \"esqueci minha senha\"");
            sn.save();
            sn = new StatusNota(6L,"O cupom foi emitido fora do Estado de São Paulo","Somente cupons e notas fiscais emitidos dentro do Estado de São Paulo podem ser doados");
            sn.save();
            sn = new StatusNota(7L,"O cupom excedeu o prazo máximo para o cadastro da doação","Os cupons e notas fiscais vencem sempre no dia 20 do mês subsequente a sua emissão, por exemplo, cupons emitidos durante o mês de janeiro vencem em 20 de fevereiro, os emitidos em fevereiro vencem em 20 de março, e assim sucessivamente");
            sn.save();
            sn = new StatusNota(8L,"O cupom doado foi classificado, como não sendo de seu próprio consumo","Devido as políticas de análise de perfil de consumo estabelecida pela secretaria da fazenda do estado de São Paulo, foi identificado que você doou cupons ou notas fiscais que provavelmente contem produtos de aquisição de outra pessoa");
            sn.save();
            sn = new StatusNota(99L,"Cupom aguardando upload","O cupom ainda não foi enviado para processamento pois não consequiu uma conexão de internet de qualidade ou está aguardando a conexão como uma rede WIFI.");
            sn.save();

        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }

//    private OkHttpClient getOkHttpClient() {
//        return new OkHttpClient.Builder()
//                .followRedirects(true)
//                .followSslRedirects(true)
//                .retryOnConnectionFailure(true)
//                .connectTimeout(15, TimeUnit.SECONDS)
//                .writeTimeout(30, TimeUnit.SECONDS)
//                .readTimeout(30, TimeUnit.SECONDS)
//
//                // you can add your own request interceptors to add authorization headers.
//                // do not modify the body or the http method here, as they are set and managed
//                // internally by Upload Service, and tinkering with them will result in strange,
//                // erroneous and unpredicted behaviors
////                .addNetworkInterceptor(new Interceptor() {
////                    @Override
////                    public Response intercept(Chain chain) throws IOException {
////                        Request.Builder request = chain.request().newBuilder()
////                                .addHeader("myheader", "myvalue")
////                                .addHeader("mysecondheader", "mysecondvalue");
////
////                        return chain.proceed(request.build());
////                    }
////                })
////
////                // open up your Chrome and go to: chrome://inspect
////                .addNetworkInterceptor(new StethoInterceptor())
////
////                // if you use HttpLoggingInterceptor, be sure to put it always as the last interceptor
////                // in the chain and to not use BODY level logging, otherwise you will get all your
////                // file contents in the log. Logging body is suitable only for small requests.
////                .addInterceptor(new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
////                    @Override
////                    public void log(String message) {
////                        Log.d("OkHttp", message);
////                    }
////                }).setLevel(HttpLoggingInterceptor.Level.HEADERS))
//
//                .cache(null)
//                .build();
//    }

    private static OkHttpClient getHttpClient(Context context) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .followRedirects(true)
                .followSslRedirects(true)
                .retryOnConnectionFailure(true)
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .cache(null);


        return enableTls12OnPreLollipop(builder).build();
    }

    private void enableStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDialog()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll()
                .penaltyLog()
                .build());
    }


    /**
     * Enables TLS 1.2 on Pre-Lollipop devices (16 >= Android API < 20)
     * Check: https://github.com/square/okhttp/issues/2372#issuecomment-244807676
     * @param client OkHttpClient.Builder on which to apply the patch
     * @return OkHttpClient.Builder with the patch applied
     */
    private static OkHttpClient.Builder enableTls12OnPreLollipop(OkHttpClient.Builder client) {
        // the problem sill persists on some samsung devices with API 21, so it's necessary
        // to apply the patch till API 21
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT < 22) {
            try {
                SSLContext sc = SSLContext.getInstance("TLSv1.2");
                sc.init(null, null, null);
                client.sslSocketFactory(new Tls12SocketFactory(sc.getSocketFactory()));

                ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_2)
                        .build();

                List<ConnectionSpec> specs =  new ArrayList<>();
                specs.add(cs);
                specs.add(ConnectionSpec.COMPATIBLE_TLS);
                specs.add(ConnectionSpec.CLEARTEXT);

                client.connectionSpecs(specs);
            } catch (Throwable exc) {
                Log.e("OkHttp", "Error while setting TLS 1.2", exc);
            }
        }

        return client;
    }
}


