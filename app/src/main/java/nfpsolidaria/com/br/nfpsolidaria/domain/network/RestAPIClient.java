package nfpsolidaria.com.br.nfpsolidaria.domain.network;

import com.google.gson.GsonBuilder;

import nfpsolidaria.com.br.nfpsolidaria.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by elcio on 20/08/17.
 */



public class RestAPIClient {

    private static Retrofit REST_ADAPTER;

    public RestAPIClient(){
        createAdapterIfNeeded();
    }

    private static void createAdapterIfNeeded() {

        if (REST_ADAPTER == null) {

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            final OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new BasicAuthInterceptor(BuildConfig.WEBSERVICEUSER, BuildConfig.WEBSERVICEPASS))
                    .build();


            REST_ADAPTER = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASEURL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setLenient().create()))
                    .build();
        }
    }


    public RestAPI getRestService() {
        return REST_ADAPTER.create(RestAPI.class);
    }

}
