package nfpsolidaria.com.br.nfpsolidaria.domain.model;

import com.orm.SugarRecord;
import com.orm.dsl.Column;

import java.io.Serializable;

/**
 * Created by elcio on 18/10/17.
 */

public class UserLocal extends SugarRecord implements Serializable {

    private String nome;
    private String cpf;
    private String dataNascimento;
    private String senhaSEFAZ;

    public UserLocal(){}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSenhaSEFAZ() {
        return senhaSEFAZ;
    }

    public void setSenhaSEFAZ(String senhaSEFAZ) {
        this.senhaSEFAZ = senhaSEFAZ;
    }

    @Override
    public String toString() {
        return "UserLocal{" +
                "nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                ", dataNascimento='" + dataNascimento + '\'' +
                ", senhaSEFAZ='" + senhaSEFAZ + '\'' +
                '}';
    }
}
